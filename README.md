# Paragraphs Colorifier

Firefox extension to colorify paragraphs, titles, etc.

### Goal

The goal of this extension is to color paragraph to help certain people to read page.
Color differences is one of the possibility to improve web accessibility.


### Authorisations needed

`storage`: The extension need to store the color option between tabs and windows.
`activeTab`: The extension need to get activeTab access to color web page.

### Version Notes

v1.1: Adding of highlighting options + Color improvment.
v1.0: First version.