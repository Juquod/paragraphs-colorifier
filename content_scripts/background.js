
/*
    ##############################
    # IMPORTS & GLOBAL VARIABLES #
    ##############################
*/

var COLORATION_PARAGRAPHS = false;  // GLOBAL variable for the paragraphs <p> coloration
var COLORATION_SPAN = false;    // GLOBAL variable for the span <span> coloration
var COLORATION_TITLES = false;  // GLOBAL variable for the titles <hX> coloration
var COLORATION_DIVS = false;    // GLOBAL variable for the divs <div> coloration
var SURLIGNAGE_PARAGRAPHS = false;   // GLOBAL variable for the paragraphs <p> surlignage
var SURLIGNAGE_SPAN = false;    // GLOBAL variable for the span <span> surlignage
var SURLIGNAGE_TITLES = false;  // GLOBAL variable for the titles <hX> surlignage
var SURLIGNAGE_DIVS = false;    // GLOBAL variable for the divs <div> surlignage

/*
    ###########
    # STORAGE #
    ###########
*/

/** Restauration of COLORATION_PARAGRAPHS value from the Sync storage (result in COLORATION_PARAGRAPHS). */
const restoreOptions_COLORATION_PARAGRAPHS = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_PARAGRAPHS = browser.storage.sync.get('COLORATION_PARAGRAPHS');
            gettingItem_COLORATION_PARAGRAPHS.then((res) => {
                COLORATION_PARAGRAPHS = res.COLORATION_PARAGRAPHS;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of COLORATION_SPAN value from the Sync storage (result in COLORATION_SPAN). */
const restoreOptions_COLORATION_SPAN = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_SPAN = browser.storage.sync.get('COLORATION_SPAN');
            gettingItem_COLORATION_SPAN.then((res) => {
                COLORATION_SPAN = res.COLORATION_SPAN;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of COLORATION_TITLES value from the Sync storage (result in COLORATION_TITLES). */
const restoreOptions_COLORATION_TITLES = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_TITLES = browser.storage.sync.get('COLORATION_TITLES');
            gettingItem_COLORATION_TITLES.then((res) => {
                COLORATION_TITLES = res.COLORATION_TITLES;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of COLORATION_DIVS value from the Sync storage (result in COLORATION_DIVS). */
const restoreOptions_COLORATION_DIVS = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_DIVS = browser.storage.sync.get('COLORATION_DIVS');
            gettingItem_COLORATION_DIVS.then((res) => {
                COLORATION_DIVS = res.COLORATION_DIVS;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of SURLIGNAGE_PARAGRAPHS value from the Sync storage (result in SURLIGNAGE_PARAGRAPHS). */
const restoreOptions_SURLIGNAGE_PARAGRAPHS = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_PARAGRAPHS = browser.storage.sync.get('SURLIGNAGE_PARAGRAPHS');
            gettingItem_SURLIGNAGE_PARAGRAPHS.then((res) => {
                SURLIGNAGE_PARAGRAPHS = res.SURLIGNAGE_PARAGRAPHS;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of SURLIGNAGE_SPAN value from the Sync storage (result in SURLIGNAGE_SPAN). */
const restoreOptions_SURLIGNAGE_SPAN = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_SPAN = browser.storage.sync.get('SURLIGNAGE_SPAN');
            gettingItem_SURLIGNAGE_SPAN.then((res) => {
                SURLIGNAGE_SPAN = res.SURLIGNAGE_SPAN;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of SURLIGNAGE_TITLES value from the Sync storage (result in SURLIGNAGE_TITLES). */
const restoreOptions_SURLIGNAGE_TITLES = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_TITLES = browser.storage.sync.get('SURLIGNAGE_TITLES');
            gettingItem_SURLIGNAGE_TITLES.then((res) => {
                SURLIGNAGE_TITLES = res.SURLIGNAGE_TITLES;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of SURLIGNAGE_DIVS value from the Sync storage (result in SURLIGNAGE_DIVS). */
const restoreOptions_SURLIGNAGE_DIVS = async () => {
    
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_DIVS = browser.storage.sync.get('SURLIGNAGE_DIVS');
            gettingItem_SURLIGNAGE_DIVS.then((res) => {
                SURLIGNAGE_DIVS = res.SURLIGNAGE_DIVS;
                resolve();
            });
        } catch(e){ reject(); }
    });
    
}

/** Restauration of every global option from the Sync storage synchronously. */
async function restoreOptions() {
    await restoreOptions_COLORATION_PARAGRAPHS();
    await restoreOptions_COLORATION_SPAN();
    await restoreOptions_COLORATION_TITLES();
    await restoreOptions_COLORATION_DIVS();
    await restoreOptions_SURLIGNAGE_PARAGRAPHS();
    await restoreOptions_SURLIGNAGE_SPAN();
    await restoreOptions_SURLIGNAGE_TITLES();
    await restoreOptions_SURLIGNAGE_DIVS();
}


/*
    ############
    # COLORIFY #
    ############
*/

var l_color_night = [ "#fa6bcf", /* Rose */ "#8d83f7", /* Blue */ "#54cbd1", /* Cyan */ "#60d154", /* Green */ "#faf755", /* Yellow */ "#d94a48"  /* Red */ ];
var l_color_light = [ "#872e7a", /* Purple */ "#2c2369", /* Blue */ "#1b5a61", /* Cyan */ "#29611b", /* Green */ "#472701", /* Brown */ "#7a1505"  /* Red */ ];
var l_color_surlignage_night = [ "#7d240f", "#705619", "#3c6e22", "#877017", "#0c6b68", "#0e1666", "#4c1059", "#591027" ];
var l_color_surlignage_light = [ "#ffd319", "#ff901f", "#ff2975", "#f222ff", "#9532fc" ];

var DARK_SITE = undefined;  // Check if global background of the website is dark or not (FALSE: LIGHT ; TRUE: DARK)

/** Check if the global background of the website is dark or not (Return the value in DARK_SITE). */
function get_Darksite(){
    let global_color = window.getComputedStyle(document.body).getPropertyValue("background-color");
    global_color = global_color.substring(4, global_color.length-1).replace(/ /g, '').split(',');
    if(parseInt(global_color[0])+parseInt(global_color[1])+parseInt(global_color[2]) <= 384) DARK_SITE=true;
    else DARK_SITE=false;
    return DARK_SITE;
}

/** Colorify the choosen divisions by tag name with a list of colors. */
async function coloration_balise(arg_tagToColor, arg_colorList) {
    let toColor = document.getElementsByTagName(arg_tagToColor);
    for (let i = 0; i < toColor.length; i++) {
        let color = arg_colorList[i%arg_colorList.length];
        toColor[i].style.color = color;
    }
}

/** Colorify the choosen divisions by tag name with a list of colors. */
async function surlignage_balise(arg_tagToColor, arg_colorList) {
    let toColor = document.getElementsByTagName(arg_tagToColor);
    for (let i = 0; i < toColor.length; i++) {
        let color = arg_colorList[i%arg_colorList.length];
        toColor[i].style.backgroundColor = color;
    }
}

/** Colorify the webpage. */
async function colorify(
    COLORATION_PARAGRAPHS = false,
    COLORATION_SPAN = false,
    COLORATION_TITLES = false,
    COLORATION_DIVS = false,
    DARK_SITE = false
    ){

    var color_theme = null;
    if(DARK_SITE){ color_theme = l_color_night; } else { color_theme = l_color_light; }

    if(COLORATION_PARAGRAPHS) coloration_balise("p", color_theme);
    if(COLORATION_SPAN) coloration_balise("span", color_theme);
    if(COLORATION_TITLES){
        coloration_balise("h1", color_theme);
        coloration_balise("h2", color_theme);
        coloration_balise("h3", color_theme);
        coloration_balise("h4", color_theme);
        coloration_balise("h5", color_theme);
        coloration_balise("h6", color_theme);
    }
    if(COLORATION_DIVS) coloration_balise("div", color_theme);

    if(DARK_SITE){ color_theme = l_color_surlignage_night; } else { color_theme = l_color_surlignage_light; }

    if(SURLIGNAGE_PARAGRAPHS) surlignage_balise("p", color_theme);
    if(SURLIGNAGE_SPAN) surlignage_balise("span", color_theme);
    if(SURLIGNAGE_TITLES){
        surlignage_balise("h1", color_theme);
        surlignage_balise("h2", color_theme);
        surlignage_balise("h3", color_theme);
        surlignage_balise("h4", color_theme);
        surlignage_balise("h5", color_theme);
        surlignage_balise("h6", color_theme);
    }
    if(SURLIGNAGE_DIVS) surlignage_balise("div", color_theme);

}


/*
    ########
    # MAIN #
    ########
*/

/** Background.js main function */
async function main(){
    try{ 

        await restoreOptions();
        colorify(COLORATION_PARAGRAPHS, COLORATION_SPAN, COLORATION_TITLES, COLORATION_DIVS, get_Darksite());

    } catch(e) {console.log("[-] Background.js error: "+e);}
}
main();