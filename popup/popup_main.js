
/*
    ##############################
    # IMPORTS & GLOBAL VARIABLES #
    ##############################
*/

import { info, error } from "./menu.js";

var COLORATION_PARAGRAPHS = false;  // GLOBAL variable for the paragraphs <p> coloration
var COLORATION_SPAN = false;    // GLOBAL variable for the span <span> coloration
var COLORATION_TITLES = false;  // GLOBAL variable for the titles <hX> coloration
var COLORATION_DIVS = false;    // GLOBAL variable for the divs <div> coloration
var SURLIGNAGE_PARAGRAPHS = false;   // GLOBAL variable for the paragraphs <p> surlignage
var SURLIGNAGE_SPAN = false;    // GLOBAL variable for the span <span> surlignage
var SURLIGNAGE_TITLES = false;  // GLOBAL variable for the titles <hX> surlignage
var SURLIGNAGE_DIVS = false;    // GLOBAL variable for the divs <div> surlignage

var NEED_RELOAD = false;

/*
    ###########
    # STORAGE #
    ###########
*/

/** Restauration of COLORATION_PARAGRAPHS value from the Sync storage (result in COLORATION_PARAGRAPHS). */
const restoreOptions_COLORATION_PARAGRAPHS = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_PARAGRAPHS = browser.storage.sync.get('COLORATION_PARAGRAPHS');
            gettingItem_COLORATION_PARAGRAPHS.then((res) => {
                COLORATION_PARAGRAPHS = res.COLORATION_PARAGRAPHS;
                if(COLORATION_PARAGRAPHS) document.getElementById("paragraphs_color").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of COLORATION_SPAN value from the Sync storage (result in COLORATION_SPAN). */
const restoreOptions_COLORATION_SPAN = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_SPAN = browser.storage.sync.get('COLORATION_SPAN');
            gettingItem_COLORATION_SPAN.then((res) => {
                COLORATION_SPAN = res.COLORATION_SPAN;
                if(COLORATION_SPAN) document.getElementById("span_color").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of COLORATION_TITLES value from the Sync storage (result in COLORATION_TITLES). */
const restoreOptions_COLORATION_TITLES = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_TITLES = browser.storage.sync.get('COLORATION_TITLES');
            gettingItem_COLORATION_TITLES.then((res) => {
                COLORATION_TITLES = res.COLORATION_TITLES;
                if(COLORATION_TITLES) document.getElementById("titles_color").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of COLORATION_DIVS value from the Sync storage (result in COLORATION_DIVS). */
const restoreOptions_COLORATION_DIVS = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_COLORATION_DIVS = browser.storage.sync.get('COLORATION_DIVS');
            gettingItem_COLORATION_DIVS.then((res) => {
                COLORATION_DIVS = res.COLORATION_DIVS;
                if(COLORATION_DIVS) document.getElementById("divs_color").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of SURLIGNAGE_PARAGRAPHS value from the Sync storage (result in SURLIGNAGE_PARAGRAPHS). */
const restoreOptions_SURLIGNAGE_PARAGRAPHS = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_PARAGRAPHS = browser.storage.sync.get('SURLIGNAGE_PARAGRAPHS');
            gettingItem_SURLIGNAGE_PARAGRAPHS.then((res) => {
                SURLIGNAGE_PARAGRAPHS = res.SURLIGNAGE_PARAGRAPHS;
                if(SURLIGNAGE_PARAGRAPHS) document.getElementById("paragraphs_surl").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of SURLIGNAGE_SPAN value from the Sync storage (result in SURLIGNAGE_SPAN). */
const restoreOptions_SURLIGNAGE_SPAN = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_SPAN = browser.storage.sync.get('SURLIGNAGE_SPAN');
            gettingItem_SURLIGNAGE_SPAN.then((res) => {
                SURLIGNAGE_SPAN = res.SURLIGNAGE_SPAN;
                if(SURLIGNAGE_SPAN) document.getElementById("span_surl").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of SURLIGNAGE_TITLES value from the Sync storage (result in SURLIGNAGE_TITLES). */
const restoreOptions_SURLIGNAGE_TITLES = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_TITLES = browser.storage.sync.get('SURLIGNAGE_TITLES');
            gettingItem_SURLIGNAGE_TITLES.then((res) => {
                SURLIGNAGE_TITLES = res.SURLIGNAGE_TITLES;
                if(SURLIGNAGE_TITLES) document.getElementById("titles_surl").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of SURLIGNAGE_DIVS value from the Sync storage (result in SURLIGNAGE_DIVS). */
const restoreOptions_SURLIGNAGE_DIVS = async () => {
    return new Promise((resolve, reject) => {
        try{
            let gettingItem_SURLIGNAGE_DIVS = browser.storage.sync.get('SURLIGNAGE_DIVS');
            gettingItem_SURLIGNAGE_DIVS.then((res) => {
                SURLIGNAGE_DIVS = res.SURLIGNAGE_DIVS;
                if(SURLIGNAGE_DIVS) document.getElementById("divs_surl").classList.add("activated");
                resolve();
            });
        } catch(e){ reject(); }
    });
}

/** Restauration of every global option from the Sync storage synchronously. */
async function restoreOptions() {
    await restoreOptions_COLORATION_PARAGRAPHS();
    await restoreOptions_COLORATION_SPAN();
    await restoreOptions_COLORATION_TITLES();
    await restoreOptions_COLORATION_DIVS();
    await restoreOptions_SURLIGNAGE_PARAGRAPHS();
    await restoreOptions_SURLIGNAGE_SPAN();
    await restoreOptions_SURLIGNAGE_TITLES();
    await restoreOptions_SURLIGNAGE_DIVS();
}

/** Saving of COLORATION_PARAGRAPHS value to the Sync storage. */
const saveOptions_COLORATION_PARAGRAPHS = async (para_colo) => {
    return new Promise((resolve, reject) => {
        try{ 
            browser.storage.sync.set({ COLORATION_PARAGRAPHS: para_colo }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Paragraph option ! (saveOptions_COLORATION_PARAGRAPHS) - "+e); reject(); }
    });
}

/** Saving of COLORATION_SPAN value to the Sync storage. */
const saveOptions_COLORATION_SPAN = async (span_colo) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ COLORATION_SPAN: span_colo }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Span option ! (saveOptions_COLORATION_SPAN) - "+e); reject(); }
    });
}

/** Saving of COLORATION_TITLES value to the Sync storage. */
const saveOptions_COLORATION_TITLES = async (title_colo) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ COLORATION_TITLES: title_colo }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Titles option ! (saveOptions_COLORATION_TITLES) - "+e); reject(); }
    });
}

/** Saving of COLORATION_DIVS value to the Sync storage. */
const saveOptions_COLORATION_DIVS = async (divs_colo) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ COLORATION_DIVS: divs_colo }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Divs option ! (saveOptions_COLORATION_DIVS) - "+e); reject(); }
    });
}

/** Saving of SURLIGNAGE_PARAGRAPHS value to the Sync storage. */
const saveOptions_SURLIGNAGE_PARAGRAPHS = async (para_surl) => {
    return new Promise((resolve, reject) => {
        try{ 
            browser.storage.sync.set({ SURLIGNAGE_PARAGRAPHS: para_surl }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Paragraph option ! (saveOptions_SURLIGNAGE_PARAGRAPHS) - "+e); reject(); }
    });
}

/** Saving of SURLIGNAGE_SPAN value to the Sync storage. */
const saveOptions_SURLIGNAGE_SPAN = async (span_surl) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ SURLIGNAGE_SPAN: span_surl }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Span option ! (saveOptions_SURLIGNAGE_SPAN) - "+e); reject(); }
    });
}

/** Saving of SURLIGNAGE_TITLES value to the Sync storage. */
const saveOptions_SURLIGNAGE_TITLES = async (title_surl) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ SURLIGNAGE_TITLES: title_surl }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Titles option ! (saveOptions_SURLIGNAGE_TITLES) - "+e); reject(); }
    });
}

/** Saving of SURLIGNAGE_DIVS value to the Sync storage. */
const saveOptions_SURLIGNAGE_DIVS = async (divs_surl) => {
    return new Promise((resolve, reject) => {
        try{
            browser.storage.sync.set({ SURLIGNAGE_DIVS: divs_surl }).then(() => { resolve(); });
        } catch(e){ error("Impossible to save the Divs option ! (saveOptions_SURLIGNAGE_DIVS) - "+e); reject(); }
    });
}


/*
    #############
    # LISTENERS #
    #############
*/

/** Add the listening of click on the extension popup menu. */
function listenForClicksOnMenu() {
    document.addEventListener("click", (e) => {

        if(!NEED_RELOAD && (e.target.id == "paragraphs_color" || e.target.id == "span_color" || e.target.id == "titles_color" || e.target.id == "divs_color"
                            || e.target.id == "paragraphs_surl" || e.target.id == "span_surl" || e.target.id == "titles_surl" || e.target.id == "divs_surl")){
            info("You will need to reload the page to apply changes !");
            document.getElementById("popup-content").style.borderTopColor = "grey";
            NEED_RELOAD = true;
        }

        if(e.target.id == "paragraphs_color"){
            if (e.target.classList.contains("activated")) saveOptions_COLORATION_PARAGRAPHS(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_COLORATION_PARAGRAPHS(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "span_color"){
            if (e.target.classList.contains("activated")) saveOptions_COLORATION_SPAN(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_COLORATION_SPAN(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "titles_color"){
            if (e.target.classList.contains("activated"))saveOptions_COLORATION_TITLES(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_COLORATION_TITLES(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "divs_color"){
            if (e.target.classList.contains("activated"))saveOptions_COLORATION_DIVS(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_COLORATION_DIVS(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "paragraphs_surl"){
            if (e.target.classList.contains("activated")) saveOptions_SURLIGNAGE_PARAGRAPHS(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_SURLIGNAGE_PARAGRAPHS(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "span_surl"){
            if (e.target.classList.contains("activated")) saveOptions_SURLIGNAGE_SPAN(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_SURLIGNAGE_SPAN(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "titles_surl"){
            if (e.target.classList.contains("activated"))saveOptions_SURLIGNAGE_TITLES(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_SURLIGNAGE_TITLES(true).then(() => { e.target.classList.add("activated"); });
        } else if(e.target.id == "divs_surl"){
            if (e.target.classList.contains("activated"))saveOptions_SURLIGNAGE_DIVS(false).then(() => { e.target.classList.remove("activated"); });
            else saveOptions_SURLIGNAGE_DIVS(true).then(() => { e.target.classList.add("activated"); });
        }

    });
}


/*
    ########
    # MAIN #
    ########
*/

/** Popup_main.js main function. */
async function main(){
    await restoreOptions(); // Restauration of the options choosen
    listenForClicksOnMenu(); // Listen for new choices
}

try{ main(); }
catch(e){ error("Popup extension error: "+e); }