
const DIV_INFO = document.getElementById("infos");

export function info(info) {
    if(info != undefined && info != null){
        if(DIV_INFO != undefined && DIV_INFO != null) DIV_INFO.textContent += "[ ] " + info + "\r\n";
        console.log("[ ] " + info);
    }
}

export function error(error) {
    if(DIV_POPUP_CONTENT != undefined && DIV_POPUP_CONTENT != null) DIV_POPUP_CONTENT.style.borderTopColor = "red";
    if(error != undefined && error != null){
        if(DIV_INFO != undefined && DIV_INFO != null) DIV_INFO.textContent += "[-] " + error + "\r\n";
        console.log("[-] " + error);
    }
}

var DIV_POPUP_CONTENT = undefined;
try{
    DIV_POPUP_CONTENT = document.getElementById("popup-content");
    DIV_POPUP_CONTENT.style.borderTopColor = "green";
} catch (e){ error(e) }




